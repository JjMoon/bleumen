import React, { Component } from 'react';
//import { View, Text } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
//import firebase from 'firebase';
//import { Header, Button, Spinner } from './components/common';
//import LoginForm from './components/LoginForm';
import SceneConnect from './sceneConnect/mainView';
import SceneOperate from './sceneOper/mainView';

class App extends Component {
  state = { loggedIn: null };

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="sceneConnect" component={SceneConnect} title="Connect" initial hideNavBar={false}
          />
          <Scene
            key="sceneOperate" component={SceneOperate} title="Operate"
          />

        </Scene>
      </Router>
    );
  }
}

export default App;

/*
<Scene
  onLeft={() => alert('Left button!')} leftTitle="Left"  // 왼쪽 커스텀 버튼...
  direction="vertical" // 아래에서 올라옴..
/>
*/
