// Soft tab
import {
  StyleSheet,
} from 'react-native';

///-------------------------  UI Components  -------------------------  버튼..
//var fontSizeBase = 14, fontSizeTitle = 17, fontSizeButton = 16, fontSizeInput = 17;
let fontSizeBase = 12, fontSizeTitle = 15, fontSizeButton = 14, fontSizeInput = 15;


// https://css-tricks.com/snippets/css/a-guide-to-flexbox/

///-------------------------  UI Components  -------------------------  버튼..
module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    //backgroundColor: '#F5FCFF',
  },
  containerTop: {
    flex: 1,
    marginTop: 55, marginLeft: 10, marginRight: 10,
    //justifyContent: 'center',
    alignItems: 'center',  // 'flex-start', //'center',
    justifyContent: 'flex-start', //  'center', // 화면의 위에 배치.. 센터로 하면 가운데로 내려옴..
    alignItems: 'stretch',
    //alignSelf: 'flex-start'
    //backgroundColor: '#F5FCFF',
  },
  containerLeftAlgin: {
    alignItems: 'flex-start'
  },
  rowContainer: {
    // flex: 1,
    flexDirection: 'row',
    alignSelf: 'stretch',
    marginTop: 3, marginBottom: 3,
    alignItems: 'center',

    // width: windowSize.width
    //alignItems: 'stretch',
    //justifyContent: 'space-between',
    //margin: 5
  },
  stateMsg: {
    fontSize: 10,
    color: '#944',
  },

  ttl: { // title
    //flex: 1,
    fontSize: fontSizeTitle,
    textAlign: 'center',
    margin: 5,
    alignSelf: 'stretch'
  },
  ttlLeft3: { // 제목 3 .. 작은 제목.
    fontWeight: '500',
    fontSize: fontSizeBase,
    textAlign: 'left',
    margin: 4,
  },

  content: {
    fontSize: fontSizeBase,
    textAlign: 'left',
    color: '#051111',
    margin: 3,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  inputCom: {  // 입력 상자
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 4,
    borderColor: '#99DD99',
    borderWidth: 1,
    borderRadius: 4,
    fontSize: fontSizeInput,
    alignSelf: 'center'
  },
  buttonRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'stretch',
    alignSelf: 'center',
  },
  listvw: {
    flex: 1,
    marginTop: 55,
    width: 200
  },
  buttonBase: {
    justifyContent: 'center',
    //alignItems: 'center',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    padding: 3,//marginTop: 10,
    margin: 5,
    alignItems: 'stretch'
  },
  buttonLabel: {
    fontWeight: '400',
    alignSelf: 'center',
    alignItems: 'stretch',
    color: '#0000FF',
    fontSize: fontSizeButton
  },
});
