/**
 */
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableHighlight } from 'react-native';

/*
사용 example =========================================================================================
<HtButton width={100} fWidth={1} ptext={'Other'}
  onPressCallback={this._bttnActOtherView.bind(this)}>
사용 example =========================================================================================
*/
//const styl = require('./HtStyles');
///-------------------------  UI Components  -------------------------  버튼..
class HtButton extends Component {
   //  {/* View -------------------------  버튼 Location  ------------------------- View */}
  render() {
    return (
      <TouchableHighlight
        style={[sty.buttonBase, { width: this.props.w, flex: this.props.f }]}
        underlayColor={'#CCDDEE'}
        onPress={this.props.onPressCallback}
      >
        <Text style={[sty.buttonLabel]}>{this.props.ptext}</Text>
      </TouchableHighlight>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  buttonBase: {
    justifyContent: 'center',
    //alignItems: 'center',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    padding: 3, //marginTop: 10,
    margin: 5,
    alignItems: 'stretch'
  },
  separator: {
    justifyContent: 'center',
    width: 10,
    //alignItems: 'center',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    padding: 3,
    //marginTop: 10,
    margin: 5,
    alignItems: 'stretch'
  },
  buttonLabel: {
    fontWeight: '400',
    alignSelf: 'center',
    alignItems: 'stretch',
    color: '#0000FF',
    fontSize: 11
  },
});

export { HtButton };

/*
parameter ::
  ptext : 버튼 라벨 텍스트
  onPressCallback : 콜백 함수
  fWidth : flex 값.
  width : 절대값 폭.
  example ________________________________________________________________________________
  <HtButton width={100} fWidth={1} ptext={'Other'}
    onPressCallback={this._bttnActOtherView.bind(this)}>
  example ________________________________________________________________________________
*/
