import React, { Component } from 'react';
import { StyleSheet, Text, TouchableHighlight } from 'react-native';

class HtRectBttn extends Component {
  render() {
    return (
      <TouchableHighlight
        style={[sty.buttonBase, {
          backgroundColor: this.props.bgCol,
          width: this.props.len,
          borderRadius: this.props.borderR,
          height: this.props.len, }]}
        underlayColor={'#CCDDEE'}
        onPress={this.props.onPressCallback}
      >
        <Text
          style={[sty.buttonLabel, {
          color: this.props.txtCol,
          fontSize: this.props.len * this.props.txtRatio }]}
        >
          {this.props.ptext}
        </Text>
      </TouchableHighlight>
    );
  }

}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  buttonBase: {
    justifyContent: 'center',
    padding: 3,
    margin: 5,
    borderRadius: 10,
    alignItems: 'center',
  },
  separator: {
    justifyContent: 'center',
    width: 10,
    alignItems: 'center',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    padding: 3,
    margin: 5,
  },
  buttonLabel: {
    fontWeight: '400',
    textAlign: 'center',
    //alignSelf: 'center',
    //alignItems: 'center',
  },
});


export { HtRectBttn };
