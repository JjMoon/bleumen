import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class SceneOperate extends Component {
  render() {
    return (
      <View style={{ marginTop: 128 }}>
        <Text onPress={Actions.pop}>This is PageTwo!</Text>
        <Text>{this.props.text}</Text>
      </View>
    );
  }
}
