import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { HtRectBttn, HtColor } from '../uicompo';

export default class SceneConnect extends Component {
  render() {
    return (
      <View style={sty.mainContainer}>
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={[sty.container, { flex: 60 }]}>
          <TouchableHighlight
            onPress={Actions.sceneOperate}
          >
          <Text>다음 화면으로</Text>
          </TouchableHighlight>

          {/*  <<<< 메인 버튼 >>>> */}

        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  mainContainer: {
    alignItems: 'stretch', justifyContent: 'center', flex: 1, margin: 0, marginTop: 20
  },
  container: {
    alignItems: 'stretch',
    justifyContent: 'center',

  },
  rowContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
  },
  seperateLine: {
    height: 2, backgroundColor: '#AAAAAA'
  },
  bttnView: {
    flex: 3,
    alignItems: 'center'
  },

  //  View Component Styles...
  titleCommon: {
    textAlign: 'center',
    alignSelf: 'stretch',
    fontWeight: '300',
    margin: 5,
  },
  title: {
    fontSize: 50,
    fontWeight: '500',
    margin: 15,
  },
  subTitle: {
    fontSize: 25,
    fontWeight: '100',
  },
  comText: {
    fontSize: 20,
    fontWeight: '100',
  },
  bttnBase: {
    width: 150, height: 150,
    backgroundColor: '#22AAEE',
    color: '#EEFFEE'
  },
  explainText: { // 설명 텍스트 스타일..
    textAlign: 'center', // ('auto', 'left', 'right', 'center', 'justify')
    fontSize: 18,
    fontWeight: '100',
    margin: 10,
    color: '#113355'
  }
});
